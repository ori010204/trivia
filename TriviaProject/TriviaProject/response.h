#include <iostream>
#pragma once

// error message
struct ErrorResponse
{
	std::string message;
};

struct LoginResponse
{
	unsigned int status;
};

struct SignupResponse
{
	unsigned int status;
};

class JsonResponsePacketSerializer
{
public:
	static char* serializeResponse(ErrorResponse);
	static char* serializeResponse(LoginResponse);
	static char* serializeResponse(SignupResponse);
};
