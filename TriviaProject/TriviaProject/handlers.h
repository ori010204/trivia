#pragma once

#include <vector>
#include <time.h>
typedef unsigned char Byte;
typedef Byte* Buffer;

struct RequestInfo
{
	unsigned int id;
	char* receivalTime;
	std::vector<Byte> buffer;
};

class IRequestHandler {
public:
	bool isRequestRelevant(RequestInfo);

	RequestResult handleRequest(RequestInfo);
};

struct RequestResult
{
	char* response;
	IRequestHandler* newHandler;
};

class LoginRequestHandler : IRequestHandler
{
public:
	LoginRequestHandler();
	~LoginRequestHandler();

	bool isRequestRelevant(RequestInfo requestInfo);
	RequestResult handleRequest(RequestInfo requestInfo);

private:
	LoginManager& m_loginManager;
	RequestHandlerFactory& m_handlerFactory;

	RequestResult login(RequestInfo requestInfo);
	RequestResult signup(RequestInfo requestInfo);
};
