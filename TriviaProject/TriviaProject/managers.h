#pragma once
#include <string>
#include <vector>

class LoggedUser
{
	std::string m_username;
public:
	std::string getUserName();
};

class LoginManager
{
	IDatabase* m_database;
	std::vector<LoggedUser> m_loggedUsers;
public:
	void signup(const std::string& username, const std::string& password, const std::string& s1);
	void login(const std::string& username, const std::string& password);
	void logout(const std::string& username);
};
