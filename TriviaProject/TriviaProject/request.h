#pragma once
#include <string>
typedef unsigned char Byte;
typedef Byte* Buffer;

struct SignupRequest
{
	std::string username;
	std::string password;
	std::string email;
};

struct LoginRequest
{
	std::string username;
	std::string password;
};

class JsonRequestPacketDeserializer
{
public:
	static LoginRequest deserializeLoginRequest(Buffer);
	static SignupRequest deserializeSignupRequest(Buffer);
};
