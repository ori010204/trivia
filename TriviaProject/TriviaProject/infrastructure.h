#pragma once
#include <WinSock2.h>
#include <string>
#include <map>
#include "handlers.h"
#include "managers.h"

class IDatabase
{
public:
	virtual bool doesUserExists(const std::string& name) const = 0;
	virtual bool doesPasswordsMatch(const std::string& pass1, const std::string& pass2) const = 0;
	virtual void addNewUser(const std::string& username, const std::string& password, const std::string& s1) const = 0;
};

struct Communicator
{
	SOCKET m_serverSocket;
	std::map<SOCKET, IRequestHandler*> m_clients;
	RequestHandlerFactory& m_handlerFactory;
public:
	void startHandlerRequests();
private:
	void bindAndListen();
	void handleNewClient(SOCKET clientSock);
};

class Server
{
public:
	void run();
private:
	IDatabase* m_database;
	Communicator m_communicator;
	RequestHandlerFactory m_handlerFactory;
};

class IDatabase
{
public:
	virtual bool doesUserExists(const std::string& name) const = 0;
	virtual bool doesPasswordsMatch(const std::string& pass1, const std::string& pass2) const = 0;
	virtual void addNewUser(const std::string& username, const std::string& password, const std::string& s1) const = 0;

};

class RequestHandlerFactory
{
public:
	LoginRequestHandler createLoginRequestHandler();
private:
	LoginManager m_loginmanager;
	IDatabase* m_database;
};
